"use strict"

/*
1. Логічні оператори використовуються для обєднання чи порівняння логічних значень.

2. Є такі логічні оператори:

( && ) - І

( || ) - АБО

( ! ) - НІ
*/

// 1 Практичне

let userAge = prompt('Enter your age: ');

if (!isNaN(userAge) && userAge !== null && userAge !== "") {
    let age = parseInt(userAge);

    if (age < 12) {
        alert('You are child.');
    }
    else if (age < 18) {
        alert('You are teenager');
    }
    else {
        alert('You are an adult');
    }

} else {
    alert('Please enter a valid number in the age field.')
}

// 2 Практичне

let month = prompt("Введіть місяць року (українською мовою, маленькими літерами):");
let daysInMonth;

switch (month) {
    case 'січень':
    case 'березень':
    case 'травень':
    case 'липень':
    case 'серпень':
    case 'жовтень':
    case 'грудень':
        daysInMonth = 31;
        break;

    case 'квітень':
    case 'червень':
    case 'вересень':
    case 'листопад':
        daysInMonth = 30;
        break;

    case 'лютий':
        daysInMonth = 28; // Без високосного
        break;

    default:
        console.log('Введено невірний місяць.');
        break;
}

daysInMonth !== undefined ? console.log(`У місяці ${month} ${daysInMonth} днів.`) : console.log('Не вдалося визначити кількість днів. Спробуйте ще раз.');
